# 오프셋과 컨슈머 랙

## 오프셋

카프카 오프셋을 이해하려면 토픽, 파티션, 프로듀서 그리고 컨슈머에 대해 알고 있어야 한다.

카프카에서 컨슈머가 데이터를 전달 받기까지 아래와 같은 과정을 거치게 된다.

- 브로커에 토픽과 파티션이 생성된다.
- 프로듀서는 특정 토픽의 파티션에 데이터를 저장한다.
- 해당 토픽의 파티션에는 프로듀서가 한동안 저장해놓은 데이터들이 쌓여있다.
- 파티션에 차곡차곡 쌓여있는 각 데이터(레코드)에는 오프셋이라는 번호가 메겨진다.
- 컨슈머는 지속해서 파티션에서 데이터를 읽어간다.
- 컨슈머는 마지막으로 읽어온 데이터의 offset을 기억한다.

## 컨슈머 랙이란?

만약 프로듀서가 데이터를 넣는 속도가 컨슈머가 읽어가는 속도보다 빠르면 어떻게 될까? 프로듀서가 마지막으로 넣은 데이터의 offset과 컨슈머가 읽은 offset이 벌어지게 될 것이다.

컨슈머 랙은 바로 이 컨슈머가 마지막으로 읽은 데이터의 offset과 프로듀서가 마지막으로 넣은 데이터의 offset의 차이값을 의미한다. 따라서 컨슈머 랙을 보면 컨슈머의 상태를 유추할 수 있다.

## 파티션이 여러개일 경우 컨슈머 랙

컨슈머 랙은 각 파티션마다 존재하기 때문에, 파티션이 여러개인 토픽에서는 컨슈머 랙도 여러개가 존재하게 된다.

한개의 토픽과 컨슈머 그룹에 대해서 가장 큰 숫자의 랙을 records-lag-max라고 부른다.

## Burrow

Burrow는 링크드인에서 개발한 오픈소스 도구로, 컨슈머의 랙을 모니터링할 수 있도록 도와주는 독립적인 애플리케이션이다.

- 멀티 카프카 클러스터를 지원한다.
  - 카프카를 여러개 운영하더라도 Burrow 하나로 모두 모니터링할 수 있다.
- Sliding window를 통한 컨슈머의 상태 확인
  - 컨슈머의 상태를 'ERROR', 'WARNING', 'OK' 표현해준다.
  - 데이터의 양이 많아 컨슈머의 오프셋이 증가하고 있으면 'WARNING'
  - 데이터의 양이 많아지는데 컨슈머가 데이터를 가져가지 않으면 'ERROR'
- HTTP API를 제공해서 Burrow에서 데이터를 조회할 수 있다.
  - API를 제공하여 다양한 추가 생태계가 구축되어 있고, 원한다면 직접 개발도 가능하다.

### Burrow을 사용해야하는 이유

컨슈머의 랙 수치는 컨슈머에서도 확인할 수 있는 값이다. 그렇다면 왜 Burrow를 사용하는 것일까?

- 컨슈머 단위로 랙을 측정하기 위해서는 추가 컨슈머를 개발할 때마다 랙을 수집하는 로직을 구현하는 것은 비효율적이다.
- 컨슈머에서 랙을 측정하고 DB에 저장하고 있다면 정작 해당 컨슈머가 다운되는 경우 랙을 확인할 수 없다.
